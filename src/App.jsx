import React, { useEffect, useState } from 'react';
import { Typography } from '@mui/material';
import useStyles from './Styles/styles';
import axios from "axios";
import exportData from './Data/key';
import { Box } from '@mui/material';
import BoardsDisplay from './Components/BoardsDisplay';
import CircularProgress from '@mui/material/CircularProgress';
import Header from './Components/Header';

function App() {
  const classes = useStyles();
  const { key, APIToken } = exportData;
  const baseUrl = `https://api.trello.com/1/members/me/boards?key=${key}&token=${APIToken}`;
  const [data, setData] = useState([]); 
  const [boardName, setBoardName] = useState('');
  const [loadingData, setLoadingData] = useState(true);
  const [error, setError] = useState(false);
 
  const getData = async () => {
    await axios.get(baseUrl)
      .then((response) => {
        setData(response.data);
        setLoadingData(false)
      }).catch((e) => {
        console.log('e')
        setError(true)
      })
  }

  const postNewBoard = async () => {
    setLoadingData(true)
    await axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${key}&token=${APIToken}`)
      .then(response => { 
        setBoardName('');
        // getData()
        setData((prev)=>[...prev , response.data])
        setLoadingData(false)
      })
      .catch(err => { setError(true); console.log('err') });
  }

  useEffect(() => {
    getData();
  }, []);
 
  const handleChange = (e) => {
    if (e.target.name == 'boardName') {
      setBoardName(e.target.value);
    }
  }

  const PostBoardData = () => {
    postNewBoard();
  }


  return (
    <Box className={classes.root}>
      <Header/>
      {error ?
        <Typography>Error 404 Not Found</Typography> :
        <>
          {loadingData ? <CircularProgress className={classes.circularLoading}  color="primary"  variant="indeterminate"  /> 
          : 
          <BoardsDisplay getData={getData} PostBoardData={PostBoardData} handleChange={handleChange} boardName={boardName} data={data} setData={setData}    />}
        </>
      }
    </Box>
  );
}

export default App;


