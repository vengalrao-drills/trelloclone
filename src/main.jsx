import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import Router from './Router.jsx'
import { BrowserRouter } from 'react-router-dom'
import { CssBaseline } from '@mui/material'

ReactDOM.createRoot(document.getElementById('root')).render(
  // <React.StrictMode>
    <BrowserRouter>
    <CssBaseline></CssBaseline>
      <Router />
    </BrowserRouter>
 
  // </React.StrictMode>,
)
