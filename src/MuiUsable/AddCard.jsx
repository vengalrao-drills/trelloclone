import { Box } from '@mui/material'
import React, { useState } from 'react'
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { Typography } from '@mui/material';
import axios from 'axios';
import exportData from '../Data/key';
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';

const AddCard = ({  text ,setCardsData,  id  }) => {
    const [newCard, setNewCard] = useState('')
    const [newCardEntry, setNewCardEntry] = useState(true);
    const { key, APIToken } = exportData;
    let textDisplay = 'Enter ChecklistName  ';
    if(text){
        textDisplay = text;
    }

    const createCard = async () => {
        await axios.post(`http://api.trello.com/1/cards?name=${newCard}&idList=${id}&key=${key}&token=${APIToken}`).then(response => {
            setCardsData(prev=>[...prev , response.data])
            setNewCard('');
        }).catch(err => {
            console.log(err)
        });
    } 

    const handleSubmit = (e) =>{
        e.preventDefault(); 
        createCard();
        setNewCardEntry(prev=>!prev)
    }

    const handleChange = (e)=>{
        setNewCard(e.target.value)
    }

    const changeView = () =>{
        setNewCardEntry(prev => !prev)
    }

    return (
        <Box  >
            {newCardEntry ?
                <Button onClick={() => changeView()} variant="text" sx={{ margin: "0 0 0 0 ", display: "flex", width: "100%" }} >
                    <Box><AddIcon sx={{ fontSize: "1.7rem", alignSelf: "self-start" }} />  </Box>
                    <Typography variant='h5' sx={{ textTransform: 'none', margin: "0 0 0 0.3rem" , flexGrow: '1', textAlign: "start" , width:"100%" }}  >Add a card</Typography>
                </Button>
                :
                <>
                    <form onSubmit={handleSubmit}  >
                    <TextField  sx={{color:"black" , width:"100%" , margin:"1rem 0 0 0 "}} required value={newCard} id="outlined-basic" label={textDisplay} variant="outlined"  onChange={(e) =>    { handleChange(e) }}  />
                    <Box sx={{margin:"1rem 0"}}> 
                        <Button variant="contained" type='submit' sx={{ }}  >Add Card</Button>
                        <Button  sx={{width:"10%" , alignItems:'flex-start' }}  onClick={() => { setNewCard('');changeView() }}> <CloseIcon></CloseIcon> </Button>
                    </Box>
                    </form>
                </>
            }
        </Box>
    )
}

export default AddCard    