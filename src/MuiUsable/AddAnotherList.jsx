import { Box, Stack } from '@mui/material'
import React, { useEffect, useState } from 'react'
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { Typography } from '@mui/material';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import exportData from '../Data/key';


const AddAnotherList = ({  id,setLists  }) => {
    const [newList, setNewList] = useState('');
    const [newListEntry, setNewListEntry] = useState(true); 

    let { key, APIToken } = exportData;

    const addNewList = async () => {
        await axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${newList}&key=${key}&token=${APIToken}`).then((response) => {
            setNewListEntry(prev => !prev);
            setNewList('')
            setLists(prev=> [...prev , response.data] );
        }).catch((e) => {
            console.log(e)
        })
    }
   

    const finalCheck = (e) => {
        e.preventDefault();
        addNewList();
    }
    const changeView = () =>{
        setNewListEntry(prev => !prev) 
    }

    return (
        <>
        <Box>
            {newListEntry ?
                <Stack direction="row" spacing={1} onClick={() => changeView() } >
                   <Typography variant='h5' sx={{ textTransform: 'none', margin: "0 0 0 0.3rem", width: "100%", flexGrow: '1', textAlign: "start" , cursor:"pointer" }}  >Enter List Name</Typography>
                </Stack>
                :
                <>
                <form onSubmit={finalCheck}> 
                <TextField  sx={{color:"black" , width:"100%" , margin:"1rem 0 0 0 " , fontSize:"1.5rem" }} required  id="outlined-basic" label="Add New List" variant="outlined" value={newList}  onChange={(e) => { setNewList(e.target.value) }}  />
                
                    <br />
                    <Box sx={{margin:"1rem 0"}} >
                        <Button variant="contained" type='submit' >  <Typography variant='h6' > Add List </Typography> </Button>
                        <Button   sx={{  alignItems:'flex-start' }} onClick={() => { setNewList(''); changeView() }}> <Typography variant='h6'>  close </Typography>   </Button>
                    </Box>
                </form> 
                </>
            }
        </Box>
        </>
    )
}

export default AddAnotherList
