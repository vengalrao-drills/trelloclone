import React, { useState } from 'react'
import { Button } from '@mui/material'
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { IconButton } from '@mui/material';
import { TextField } from '@mui/material';

const AddCheckList = ({ createCheckList }) => {

    const [inputCheckList, setInputCheckList] = useState('');
    const [inputDisplay, setInputDisplay] = useState(false);

    const handleChange = (e) => {
        setInputCheckList(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        createCheckList(inputCheckList);
        setInputCheckList('');
        setInputDisplay(false);
    };
    const displayChange = () => {
        setInputDisplay(prev => !prev)
    }


    return (
        <>
            {inputDisplay ? (
                <Box sx={{margin:'auto'}}>
                    <form onSubmit={handleSubmit}>
                        <TextField sx={{ width:"100%" }} required type='text' value={inputCheckList} label="Enter CheckList Name"
                            onChange={(e) => handleChange(e)} />
                        <Box>

                            <Button type="submit"   >
                                <Typography variant='h5' sx={{ padding: " 0 1rem", alignItems: "center" }}  >Submit</Typography>
                            </Button>
                            <IconButton onClick={() => displayChange()} sx={{ borderRadius: "0" }}  >
                                <Typography variant='h5'>  Cancel  </Typography>   <CloseIcon sx={{ color: 'red', fontSize: "2rem" }} />
                            </IconButton>
                        </Box>
                    </form>
                </Box>
            ) : (
                <Button onClick={() => displayChange()}>
                    <Typography variant='h5' sx={{ padding: "0.5rem" }}  >Create Checklist</Typography>
                </Button>
            )}

        </>
    )
}

export default AddCheckList