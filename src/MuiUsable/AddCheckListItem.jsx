import { Box } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';
import React, { useState } from 'react'
import Button from '@mui/material/Button';
import { Typography } from '@mui/material';
import axios from 'axios';
import exportData from '../Data/key';
import TextField from '@mui/material/TextField';

const AddCheckListItem = ({ checkListData, setCheckListData, setItem, item, newCheckListItem, setNewCheckListItem, getCheckLists }) => {
    const [newCheckList, setNewCheckList] = useState(true);

    const addNewChecklist = async (id) => {
        const { key, APIToken } = exportData;
        // console.log(id)
        await axios.post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${newCheckListItem}&key=${key}&token=${APIToken}`).then(response => {
            setItem({ ...item, ['checkItems']: [...item.checkItems, response.data] })

            let values = checkListData.map((checklistss, index) => {
                if (checklistss.id == id) {
                    console.log(checklistss.checkItems)

                    checklistss.checkItems.push(response.data);
                    return checklistss
                }
                return checklistss
            })
            setCheckListData(values)

        }).catch((e) => {
            console.log(e)
        })
    }

    const submitForm = (e) => {
        e.preventDefault();
        addNewChecklist(item.id);
        setNewCheckListItem('');
        setNewCheckList(prev => !prev)
    }
    return (
        <div>
            <>
                {newCheckList ?
                    <Button onClick={() => setNewCheckList(prev => !prev)} variant="text" sx={{ margin: "rem 0 0 0 ", display: "flex", width: "100%" }} >
                        <Box>
                            <AddIcon sx={{ fontSize: "1.7rem", alignSelf: "self-start" }} />  </Box>
                        <Typography variant='h5' sx={{ textTransform: 'none', margin: "0 0 0 0.3rem", width: "100%", flexGrow: '1', textAlign: "start" }}  >Enter CheckItem</Typography>
                    </Button>
                    :
                    <>
                        <TextField sx={{ margin: "1rem 0" }} label="Enter CheckItem Name" required type="text" value={newCheckListItem} onChange={(e) => { setNewCheckListItem(e.target.value) }} id="filled-basic" variant="filled" />
                        <br />
                        <form action="">
                            <Button variant="contained" onClick={(e) => { submitForm(e) }}  >Add  </Button>
                            <Button onClick={() => setNewCheckList(prev => !prev)} > close </Button>
                        </form>

                    </>
                }
            </ >
        </div>
    )
}

export default AddCheckListItem
