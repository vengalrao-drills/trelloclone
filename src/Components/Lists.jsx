import React, { useState } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import Header from './Header';
import { Typography } from '@mui/material';
import { Stack } from '@mui/material';
import axios from "axios";
import exportData from "../Data/key";
import { Box } from '@mui/material'; 
import CardMui from '../MuiUsable/CardMui';
 

const Lists = () => {
    const location = useLocation();
    let currentBoardName = location.state

    let { id } = useParams();
    let [lists, setLists] = useState([]); 
    const getLists = async () => {
        let { key, APIToken } = exportData;
        await axios
            .get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${APIToken}`)
            .then((response) => {
                // console.log(response.data)
                setLists(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    }

    useState(() => {
        getLists()
    }, [id])


    return (
        <div>
            <Header />
            <Box> 
                <Box sx={{display:"flex", alignItems:"baseline"}}>
                    <Typography variant='h3' sx={{margin:"2rem auto",textAlign:"center"  }} > 
                        {currentBoardName}
                    </Typography>
                </Box>  
                <CardMui lists ={lists} setLists={setLists}    />    
            </Box>

        </div>
    )
}

export default Lists






// <Box>
// {/* <Stack   direction="row" className={classes.listsMap} > */}
// <CardMui lists ={lists} /> 
    
//     {/* {lists.map((list) => (
//        <CardContent  key={list.id} className={classes.individualList}>
//             <Typography variant='h6'  >{list.name}  </Typography>                                
//             <Typography variant='h7'  > {list.id} </Typography>
//           </ CardContent >  
//     ))} */}
// {/* </Stack> */}
// </Box>
