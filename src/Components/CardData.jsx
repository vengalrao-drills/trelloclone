import React, { useEffect, useState } from 'react'
import axios from 'axios';
import exportData from '../Data/key';
import { Card, Typography } from '@mui/material';
import { CardContent } from '@mui/material';
// import useStyles from '../Styles/styles';
import { Button } from '@mui/material';
import { Box } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import CheckListsDisplay from '../MuiUsable/CheckListsDisplay';
import AddCard from '../MuiUsable/AddCard';

const CardData = ({ id  }) => {
    const { key, APIToken } = exportData;
    let [cardsData, setCardsData] = useState([]);
    const getCards = async () => { 
        console.log('getcards function called')
        await axios
            .get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${APIToken}`)
            .then((response) => { 
                // console.log(response.data)
                setCardsData(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    useEffect(()=>{
        getCards()
    },[])

    const deleteCard = (ID) => {
          axios.delete(`https://api.trello.com/1/cards/${ID}?key=${key}&token=${APIToken}`).then(response => {
            setCardsData(cardsData.filter(card => card.id !== ID)); 
        }).catch((e) => {
            console.log(e)
        })
    }

    let cardDataDisplay = '';
    if (cardsData.length > 0) {
        cardDataDisplay = cardsData.map((card) => {
            // console.log('card ', card)
            return (
                <Box key={card.id}   >
                    <Card sx={{ margin: "0.9rem 0rem ", borderRadius: "0.5rem" }}  >
                        <CardContent className='{classes}' sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}  >
                            <CheckListsDisplay getCards={getCards} cardId={card.id} cardName={card.name}   >
                                <Typography variant='h6' sx={{ }} onClick={()=>getCards()} >{card.name}   </Typography>
                            </CheckListsDisplay>
                            <Button onClick={() => { deleteCard(card.id) }}  >  <DeleteIcon sx={{ color: "black" }} />     </Button>
                        </CardContent>
                    </Card>
                </Box>
            )
        });
    }

    return (
        <div>
            {cardDataDisplay}
            <AddCard text={'Enter Card Name'} setCardsData={setCardsData}  id={id}    />   
        </div>
    )
}

export default CardData  
