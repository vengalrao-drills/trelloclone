import { Box } from '@mui/material';
import React, { useEffect, useState } from 'react';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import { Card } from '@mui/material';
import AddCheckListItem from '../MuiUsable/AddCheckListItem';
import exportData from '../Data/key';
import axios from 'axios';
import { Button } from '@mui/material'; 
import CloseIcon from '@mui/icons-material/Close'; 

const CheckItems = ({ work, getCheckLists, checkListData, setCheckListData }) => {
  const [item, setItem] = useState(work);
  const { key, APIToken } = exportData;
  const [newCheckListItem, setNewCheckListItem] = useState('');

  const [checkItemStates, setCheckItemStates] = useState(
    item.checkItems.map(item => item.state === 'complete')
  );
  // const [val ,setVal ] = useState(
  //   item.checkItems.filter(item => item.state === 'complete')
  // )
  // console.log('val' , val)
 
  const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
  let newCheckItemStates=[]

  const handleToggle = (index, chkItem) => {
    newCheckItemStates = checkItemStates.map((item, i) => (i === index ? !item : item));
    setCheckItemStates(newCheckItemStates);
    let state = 'complete'
    if (newCheckItemStates[index] == true) {
      state = 'complete'
    } else {
      state = 'incomplete'
    }
    updateChkItem(item.idCard, chkItem.idChecklist, chkItem.id, state)
  };

  const updateChkItem = async  (idCard , idChecklist,idCheckItem, state) => {
    await axios.put(`https://api.trello.com/1/cards/${idCard}/checklist/${idChecklist}/checkItem/${idCheckItem}?key=${key}&token=${APIToken}&state=${state}`).then(response => {
    getCheckLists();
    }).catch((e) => {
      console.log(e)
    })
  }
  const deleteCheckItem = (checkItemid) => {
    axios.delete(`https://api.trello.com/1/checklists/${checkItemid.idChecklist}/checkItems/${checkItemid.id}?key=${key}&token=${APIToken}`).then(response => {
      let localData = item.checkItems.filter(checkItems => checkItems.id != checkItemid.id)
      setItem({ ...item, 'checkItems': localData });
      let values = checkListData.map((checklistss, index) => {
        if (checklistss.id == checkItemid.idChecklist) {
          let data = checklistss.checkItems.filter(allCheckItem => allCheckItem.id != checkItemid.id)
          checklistss.checkItems = data
          return checklistss
        }
        return checklistss
      })
      setCheckListData(values);
    }).catch(e => {
      console.log(e)
    })
  }

  return (
    <>
    <Card   >
      {item.checkItems.map((items, index) => (
        <Box key={index} sx={{ display: "flex", justifyContent: "space-between", "&:hover .deleteBtn": { display: 'block' } }} >
          <Box sx={{ display: "flex", alignItems: 'center', "&:hover .deleteBtn": { display:"flex", justifyContent:"center", alignItems:"center" } }}>
            <Checkbox  {...label} checked={checkItemStates[index]}
              onChange={() => handleToggle(index, items)}
            />
            <Typography variant='h5'  >
              {items.name}  
            </Typography>
          </Box>

          <Button sx={{ display: "none", color: "black" ,}} className='deleteBtn' onClick={(e) => deleteCheckItem(items)}>
           <CloseIcon sx={{margin:"auto"}}  />
          </Button>

        </Box>
      ))}
      <Box>
        <AddCheckListItem text={"Enter ChecklistName"} checkListData={checkListData} setCheckListData={setCheckListData} setItem={setItem} getCheckLists={getCheckLists} item={item} newCheckListItem={newCheckListItem} setNewCheckListItem={setNewCheckListItem} />
      </Box>
    </ Card>

    </>
  );
};

export default CheckItems; 